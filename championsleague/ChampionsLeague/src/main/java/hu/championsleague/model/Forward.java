/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.championsleague.model;

import hu.championsleague.model.constant.PreferredSide;
import hu.championsleague.utility.RandomCollection;

/**
 *
 * @author Niki
 */
public class Forward extends AbstractPlayer {

    public Forward() {
        this.randomCollection = new RandomCollection();
        randomCollection.addElement("Goalkeeper", 20);
        randomCollection.addElement("Defender", 80);
    }

    /**
     * *
     * @param opponentTeam
     * @return true if the player scored, false if lost the ball.
     */
    @Override
    public boolean attack(int roundCounter, Team opponentTeam) {

        return super.attack(roundCounter, opponentTeam);

    }
}
