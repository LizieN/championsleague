/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.championsleague.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Niki
 */
public class ScoreBoard {
    
    List<Score> scores = new ArrayList<>();
    
    public void addScore(Score score) {
        scores.add(score);
    }
    
    public void listScores() {
        for (Score score : scores) {
            System.out.println(score.toString());
        }
    }


    
}
