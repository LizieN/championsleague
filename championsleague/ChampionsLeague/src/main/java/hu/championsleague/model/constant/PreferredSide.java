/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.championsleague.model.constant;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Niki
 */
public enum PreferredSide {
    @SerializedName("left")
    LEFT,
    @SerializedName("right")
    RIGHT
}
