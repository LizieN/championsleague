/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.championsleague.model.constant;

/**
 *
 * @author Niki
 */
public enum PlayerType {
    GOALKEEPER,
    MIDFIELDER,
    FORWARD,
    DEFENDER;
}
