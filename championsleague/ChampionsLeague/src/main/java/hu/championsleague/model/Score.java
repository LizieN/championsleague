/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.championsleague.model;

/**
 *
 * @author Niki
 */
public class Score {
    
   private Team team;
   private Team opponent;
   private int teamScore;
   private int opponentScore;
   private char result; // W, D, L

    public Score(Team team, Team opponent, int teamScore, int opponentScore, char result) {
        this.team = team;
        this.opponent = opponent;
        this.teamScore = teamScore;
        this.opponentScore = opponentScore;
        this.result = result;
    }

    @Override
    public String toString() {
        return "Score{" + "team=" + team.getTeamName() + ", opponent=" + opponent.getTeamName() + ", teamScore=" + teamScore + ", opponentScore=" + opponentScore + ", result=" + result + '}';
    }
   
    
   


   
   
    
}
