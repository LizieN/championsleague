/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.championsleague.model;

import hu.championsleague.model.constant.PreferredSide;
import hu.championsleague.utility.RandomCollection;
import hu.championsleague.utility.RandomGenerator;
import lombok.Data;

/**
 *
 * @author Niki
 */
@Data
public abstract class AbstractPlayer {

    private String name;
    private int age; // Must be between 16 and 38
    private PreferredSide preferredSide;
    private int score; // From 1 to 100
    private int numberOfGoals = 0;
    protected RandomCollection randomCollection;

    public boolean attack(int roundCounter, Team opponentTeam) { // Returns true if the player scored.

        if (youngPlayerLoseTheBall(this)) {
            return false;
        }

        switch (randomCollection.getRandomElement()) {
            case "Goalkeeper":
                System.out.println("PICKED GOALKEEPER");
                if (duel(roundCounter, this, opponentTeam.getGoalkeeper())) {
                    System.out.println(this.getName() + "(" + this.getAge() + ")" + " scored!");
                    return true;
                } else {
                    System.out.println(this.getName() + "(" + this.getAge() + ")" + " lost the ball!");
                }
                break;
            case "Defender":
                System.out.println("PICKED DEFENDER");
                if (duel(roundCounter, this, opponentTeam.getRandomDefender())) {
                    System.out.println(this.getName() + "(" + this.getAge() + ")" + " has the ball!");
                    if (duel(roundCounter, this, opponentTeam.getGoalkeeper())) {
                        System.out.println(this.getName() + "(" + this.getAge() + ")" + " scored!");
                        return true;
                    } else {
                        System.out.println(this.getName() + "(" + this.getAge() + ")" + " lost the ball!");
                    }
                }
                break;
            case "Midfielder":
                System.out.println("PICKED MIDFIELDER");
                if (duel(roundCounter, this, opponentTeam.getRandomMidfielder())) {
                    System.out.println(this.getName() + "(" + this.getAge() + ")" + " has the ball!");
                    if (duel(roundCounter, this, opponentTeam.getRandomDefender())) {
                        System.out.println(this.getName() + "(" + this.getAge() + ")" + " has the ball!");
                        if (duel(roundCounter, this, opponentTeam.getGoalkeeper())) {
                            System.out.println(this.getName() + "(" + this.getAge() + ")" + " scored!");
                            return true;
                        } else {
                            System.out.println(this.getName() + "(" + this.getAge() + ")" + " lost the ball!");
                        }
                    }
                } else {
                    System.out.println(this.getName() + "(" + this.getAge() + ")" + " lost the ball!");
                }
                break;
            case "LoseTheBall":
                System.out.println("LOST");
                System.out.println(this.getName() + "(" + this.getAge() + ")" + " lost the ball!");
                break;
        }
        return false;
    }

    protected int calculateChanceToWin(int roundCounter, AbstractPlayer opponent) {
        int chance = 50; // Every player has 50% chance to win
        chance += (this.getScore() + opponent.getScore()) / 2; // + 1% for every 2 point difference
        if (this.getAge() > 32) { // old player's modifier
            chance -= (2 * (roundCounter - 1) * (this.getAge() - 32));  // <fent kiszámított>%-(2%*(<csapat támadásainak száma>-1)*(<kor>-32))
        }
        return chance;
    }

    protected boolean duel(int roundCounter, AbstractPlayer player, AbstractPlayer opponentPlayer) {
        if (RandomGenerator.generateRandomInt(1, 100) < calculateChanceToWin(roundCounter, opponentPlayer)) {
            return true;
        }
        return false;
    }

    protected boolean youngPlayerLoseTheBall(AbstractPlayer player) {

        // Young players (16-21) has 10% chance / year to loose the ball. ( (22-<age>)*10% )
        if (player.getAge() < 22) {
            if (RandomGenerator.generateRandomInt(1, 100) < (22 - this.getAge()) * 10) {
                System.out.println(this.getName() + "(" + this.getAge() + ")" + " lost the ball! X");
                return true;
            }
        }
        return false;
    }
}
