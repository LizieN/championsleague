package hu.championsleague.model.read;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataReader {
    
    public static List<String> readJsonFiles(String directoryName) {
        
        List<String> stringList = new ArrayList();
        
        try {
            File[] files = new File(directoryName).listFiles((File dir1, String name) -> name.endsWith(".json"));
            for (File file : files) {
                stringList.add(new String(Files.readAllBytes(Paths.get(directoryName + "\\" + file.getName()))));
            }
        } catch (IOException ex) {
            Logger.getLogger(DataReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return stringList;
    }
    
}
