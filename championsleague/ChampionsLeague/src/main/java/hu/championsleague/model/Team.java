package hu.championsleague.model;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.Data;

@Data
public class Team {

    private String teamName;
    private String city;
    private int teamScore;

    private Goalkeeper goalkeeper;
    private List<Defender> defenders;
    private List<Midfielder> midfielders;
    private List<Forward> forwards;

    public Team(String teamName, String city, Goalkeeper goalkeeper, List<Defender> defenders, List<Midfielder> midfielders, List<Forward> forwards) {
        this.teamName = teamName;
        this.city = city;        
        this.goalkeeper = goalkeeper;
        this.defenders = defenders;
        this.midfielders = midfielders;
        this.forwards = forwards;
        this.teamScore = calculateTeamScore();
    }

    
    
    
    public Defender getRandomDefender() {
        return defenders.get(new Random().nextInt(defenders.size()));
    }
    
    public Midfielder getRandomMidfielder() {
        return midfielders.get(new Random().nextInt(midfielders.size()));
    }
    
    public Forward getRandomForward() {
        return forwards.get(new Random().nextInt(forwards.size()));
    }
    
    public AbstractPlayer getRandomAttacker() {
        return Stream.concat(forwards.stream(), midfielders.stream())
                             .collect(Collectors.toList()).get(new Random().nextInt(forwards.size() + midfielders.size()));
    }

    private int calculateTeamScore() {
        int ts = 0;
        ts += goalkeeper.getScore();
        for (Defender defender : defenders) {
            ts += defender.getScore();
        }
        for (Midfielder midfielder : midfielders) {
            ts += midfielder.getScore();
        }
        for (Forward forward : forwards) {
            ts += forward.getScore();
        }
        return ts;
    }
    
    public int getTeamScore() {
        teamScore = calculateTeamScore();
        return teamScore;
    }
    

    @Override
    public String toString() {
        return "Team{" + "name=" + teamName + ", city=" + city + ", teamScore=" + teamScore + ", goalkeeper=" + goalkeeper + ", defenders=" + defenders + ", midfielders=" + midfielders + ", forwards=" + forwards + '}';
    }

}
