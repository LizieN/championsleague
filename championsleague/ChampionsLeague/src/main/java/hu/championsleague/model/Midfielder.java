package hu.championsleague.model;

import hu.championsleague.utility.RandomGenerator;
import hu.championsleague.model.constant.PreferredSide;
import hu.championsleague.utility.RandomCollection;

public class Midfielder extends AbstractPlayer {

    public Midfielder() {
        this.randomCollection = new RandomCollection();
        randomCollection.addElement("Goalkeeper", 40);
        randomCollection.addElement("Defender", 30);
        randomCollection.addElement("Midfielder", 20);
        randomCollection.addElement("LoseTheBall", 10);
    }

    /**
     * @param opponentTeam
     * @return true if the player scored, false if lost the ball.
     */
    @Override
    public boolean attack(int roundCounter, Team opponentTeam) {

        return super.attack(roundCounter, opponentTeam);

    }

}
