/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.championsleague.main;

import hu.championsleague.model.Team;
import hu.championsleague.model.read.DataReader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Niki
 */
public class Application {
    
    public static void main(String[] args) {

        String dataSource = "D:\\Java Projects\\championsleague\\ChampionsLeague\\data";
        
        try {
            final List<String> teamJsons = DataReader.readJsonFiles(dataSource);
            ChampionsLeague championsLeague = new ChampionsLeague();
            championsLeague.loadTeams(teamJsons);
            for (Team t : championsLeague.getTeams()) {
                System.out.println(t);
                System.out.println(t.getTeamScore());
            }
            championsLeague.start();
        } catch (Exception ex) {
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}