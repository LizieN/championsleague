/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.championsleague.main;

import com.google.gson.Gson;
import hu.championsleague.model.PlayerScoreKey;
import hu.championsleague.model.ScoreBoard;
import hu.championsleague.model.Team;
import hu.championsleague.utility.TeamValidator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Niki
 */
public final class ChampionsLeague {

    private List<Team> teams = new ArrayList<>();
    private ScoreBoard scoreBoard = new ScoreBoard();
    private Map<PlayerScoreKey, Integer> playerScoreBoard = new HashMap<>();

    public void loadTeams(List<String> teamData) throws Exception {
        Gson gson = new Gson();
        for (String string : teamData) {
            Team t = gson.fromJson(string, Team.class);
            if (TeamValidator.validateTeam(t)) {
                teams.add(t);
            } else {
                throw new Exception("Invalid team parameters!");
            }
        }
    }

    public void start() {
        for (int i = 0; i < teams.size(); i++) {
            for (int j = 0; j < teams.size(); j++) {
                if (i != j) {
                    Team homeTeam = teams.get(i);
                    Team opponentTeam = teams.get(j);
                    FootballMatch footballMatch = new FootballMatch(homeTeam, opponentTeam);
                    System.out.println("HOME TEAM: " + homeTeam);
                    System.out.println("OPPONENT TEAM: " + opponentTeam);
                    footballMatch.playMatch();
                    footballMatch.saveResult(scoreBoard);
                    final Map<PlayerScoreKey, Integer> playerScores = footballMatch.getPlayerScores();
//                    playerScores.keySet().stream().forEach(psk> );
                }
            }
        }
        scoreBoard.listScores();
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void increasePlayerScore(PlayerScoreKey key) {
        Integer playerScore = playerScoreBoard.get(key);
        if (playerScore == null) {
            playerScoreBoard.put(key, 1);
        } else {
            playerScoreBoard.put(key, ++playerScore);
        }
    }

}
