/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.championsleague.main;

import hu.championsleague.model.AbstractPlayer;
import hu.championsleague.model.PlayerScoreKey;
import hu.championsleague.model.Score;
import hu.championsleague.model.ScoreBoard;
import hu.championsleague.model.Team;
import hu.championsleague.utility.RandomGenerator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javafx.util.Pair;

/**
 *
 * @author Niki
 */
public class FootballMatch {

    private Team homeTeam;
    private Team opponentTeam;
    private int homeTeamScore = 0;
    private int opponentTeamScore = 0;
    private Map<PlayerScoreKey,Integer> playerScores = new HashMap<>();

    public FootballMatch(Team homeTeam, Team opponentTeam) {
        this.homeTeam = homeTeam;
        this.opponentTeam = opponentTeam;
    }

    private final int scoreModifierForHomeTeamPlayers = 5; // Home team gets +5 points / player.

    public void playMatch() {
        // Both team leads 3-9 attack
        int min = 3;
        int max = 9;
        int homeTeamAttacks = RandomGenerator.generateRandomInt(min, max);
        int opponentTeamAttacks = RandomGenerator.generateRandomInt(min, max);
        for (int i = 0; i < homeTeamAttacks; i++) {
            System.out.println("Home team attacks: " + i);
            AbstractPlayer randomAttacker = homeTeam.getRandomAttacker();
            randomAttacker.setScore(randomAttacker.getScore()+scoreModifierForHomeTeamPlayers);
            if (randomAttacker.attack(i, opponentTeam)) {
                    homeTeamScore++;
                    
            }
            randomAttacker.setScore(randomAttacker.getScore()-scoreModifierForHomeTeamPlayers);
        }
        
        for (int i = 0; i < opponentTeamAttacks; i++) {
            System.out.println("Opponent team attacks: " + i);
            if (opponentTeam.getRandomAttacker().attack(i, homeTeam)) {
                opponentTeamScore++;
            }

        }
    }

    public Map<PlayerScoreKey, Integer> getPlayerScores() {
        return playerScores;
    }
    
    public void saveResult(ScoreBoard scoreBoard) {
        if (homeTeamScore > opponentTeamScore) {
            scoreBoard.addScore(new Score(homeTeam, opponentTeam, homeTeamScore, opponentTeamScore, 'W'));
            scoreBoard.addScore(new Score(opponentTeam, homeTeam, opponentTeamScore, homeTeamScore, 'L'));
        } else if (opponentTeamScore > homeTeamScore) {
            scoreBoard.addScore(new Score(homeTeam, opponentTeam, homeTeamScore, opponentTeamScore, 'L'));
            scoreBoard.addScore(new Score(opponentTeam, homeTeam, opponentTeamScore, homeTeamScore, 'W'));
        } else {
            scoreBoard.addScore(new Score(homeTeam, opponentTeam, homeTeamScore, opponentTeamScore, 'D'));
            scoreBoard.addScore(new Score(opponentTeam, homeTeam, opponentTeamScore, homeTeamScore, 'D'));
        }

    }

}
