/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.championsleague.utility;

import hu.championsleague.model.Team;

/**
 *
 * @author Niki
 */
public class TeamValidator {

    public static boolean validateTeam(Team team) {
        if (team.getGoalkeeper() == null) {
            return false;
        }
        if (team.getDefenders().size() + team.getMidfielders().size() + team.getForwards().size() != 10) {
            return false;
        }
        if (team.getDefenders().size() > 5 || team.getDefenders().size() < 3) {
            return false;
        }
        if (team.getForwards().size() > 3 || team.getForwards().size() < 1) {
            return false;
        }
        return true;
    }
      public static boolean validatePlayers(Team team) {
        return true;
    }
    
    
}
