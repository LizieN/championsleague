/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.championsleague.utility;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 *
 * @author Niki
 */
public class RandomCollection {

    private int totalWeight = 0;

    private final List<Element> elements = new ArrayList();

    @Data
    private class Element {

        String elementName;
        int weigth;

        public Element(String itemName, int weigth) {
            this.elementName = itemName;
            this.weigth = totalWeight;
        }
    }

    public void addElement(String elementName, int weight) {
        totalWeight += weight;
        elements.add(new Element(elementName, totalWeight));
    }

    public String getRandomElement() {
        int r = RandomGenerator.generateRandomInt(0, totalWeight);
        for (Element element : elements) {
            if (element.getWeigth() >= r) {
                return element.getElementName();
            }
        }
        return null; // Only happens if there are no elements
    }
}
